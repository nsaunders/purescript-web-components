module FancyButton.Main where

import Prelude
import Data.Maybe (Maybe(..), isJust)
import Effect (Effect)
import Web.Component.CustomElement (CustomElement, customElement)
import Web.Component.CustomElementRegistry (define)
import Web.Component.DOM (Encapsulation(Open), attachShadow, shadowRoot)
import Web.Component.Window (customElements) as W
import Web.DOM.Document (Document, createElement)
import Web.DOM.Element (Element, getAttribute, removeAttribute, setAttribute, toNode, toParentNode)
import Web.DOM.Node (appendChild, setTextContent)
import Web.DOM.ParentNode (QuerySelector(..), querySelector)
import Web.HTML (window) as H
import Web.HTML.HTMLDocument (toDocument) as H
import Web.HTML.Window (document) as W

main :: Effect Unit
main = do
  window <- H.window
  document <- H.toDocument <$> W.document window
  customElements <- W.customElements window
  define "fancy-button" (fancyButton document) customElements

fancyButton :: Document -> CustomElement
fancyButton document =
  customElement
    { init
    , observedAttributes: ["disabled"]
    , attributeChangedCallback
    , connectedCallback
    , disconnectedCallback: const $ pure unit
    }
  where

    stylesheet :: String
    stylesheet = normalStyle <> disabledStyle
      where
      
        normalStyle :: String
        normalStyle = "button { outline: 0; padding: 8px; border-radius: 8px; color: white; background: blue; }"

        disabledStyle :: String
        disabledStyle = "button:disabled { color: rgba(255, 255, 255, 0.7); }"

    init :: Element -> Effect Unit
    init el = do
      shadow <- toNode <$> attachShadow { mode: Open } el
      style <- toNode <$> createElement "style" document
      setTextContent stylesheet style
      button <- toNode <$> createElement "button" document
      slot <- toNode <$> createElement "slot" document
      _ <- appendChild slot button
      _ <- appendChild button shadow
      _ <- appendChild style shadow
      pure unit

    attributeChangedCallback :: String -> Maybe String -> Maybe String -> Element -> Effect Unit
    attributeChangedCallback attribute oldValue newValue el =
      case attribute of
        "disabled" ->
          setButtonDisabled (flag newValue) el
        _ ->
          pure unit

    connectedCallback :: Element -> Effect Unit
    connectedCallback el = do
      disabled <- flag <$> getAttribute "disabled" el
      setButtonDisabled disabled el

    setButtonDisabled :: Boolean -> Element -> Effect Unit
    setButtonDisabled disabled el = do
      shadow <- shadowRoot el
      maybeButton <- querySelector (QuerySelector "button") (toParentNode shadow)
      case maybeButton of
        Just button -> do
          if disabled
            then setAttribute "disabled" "disabled" button
            else removeAttribute "disabled" button
          pure unit
        Nothing ->
          pure unit
 
    flag :: forall a. Maybe a -> Boolean
    flag = isJust
