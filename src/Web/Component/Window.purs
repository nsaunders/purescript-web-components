module Web.Component.Window (customElements) where

import Effect (Effect)
import Web.Component.CustomElementRegistry (CustomElementRegistry)
import Web.HTML.Window (Window)

foreign import customElements :: Window -> Effect CustomElementRegistry
