"use strict";

exports._customElement = function (spec) {
  function CustomElement() {
    var el = Reflect.construct(HTMLElement, [], CustomElement);
    spec.init(el).call();
    return el;
  }

  CustomElement.prototype = Object.create(HTMLElement.prototype);

  CustomElement.observedAttributes = spec.observedAttributes;

  CustomElement.prototype.attributeChangedCallback = function() {
    var params = [].slice.call(arguments, 0, 3).concat(this);
    for (var work = spec.attributeChangedCallback, i = 0; work instanceof Function; work = work(params[i]), i++) ;
  };
 
  CustomElement.prototype.connectedCallback = function() {
    spec.connectedCallback(this).call();
  };
  
  return CustomElement;
};
