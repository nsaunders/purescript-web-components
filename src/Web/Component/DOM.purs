module Web.Component.DOM (Encapsulation(..), ShadowRootInit, attachShadow, shadowRoot) where

import Prelude
import Effect (Effect)
import Web.DOM.Element (Element)

data Encapsulation = Open | Closed

encapsulationToDOM :: Encapsulation -> String
encapsulationToDOM Open = "open"
encapsulationToDOM Closed = "closed"

type ShadowRootInit = { mode :: Encapsulation }
type ShadowRootInitDOM = { mode :: String }

shadowRootInitToDOM :: ShadowRootInit -> ShadowRootInitDOM
shadowRootInitToDOM { mode } = { mode: encapsulationToDOM mode }

attachShadow :: ShadowRootInit -> Element -> Effect Element
attachShadow = _attachShadow <<< shadowRootInitToDOM

foreign import _attachShadow :: ShadowRootInitDOM -> Element -> Effect Element

foreign import shadowRoot :: Element -> Effect Element
